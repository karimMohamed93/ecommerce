-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 06, 2020 at 04:06 PM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommercedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'abdelkarim', 'abdelkarim@test.com', '$2y$10$StvCvKs9J0SvbDyYjxgWsu0WZwkRwn3SbMeFpD2F0oB.Zn.8g9AEO', 'MqWSTylFP8Z3F0A7QfmvKk5NPbPPIACnZ97IdKzaEp28i3ERx2GMDSKZ6xfR', '2019-12-24 08:42:46', '2020-06-28 07:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `city_name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name_ar`, `city_name_en`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'القاهرة', 'cairo', 3, '2020-01-16 11:56:33', '2020-01-16 11:56:33'),
(2, 'جدة', 'Jeddah', 4, '2020-01-16 11:57:05', '2020-01-16 11:57:05');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name_ar`, `name_en`, `color`, `created_at`, `updated_at`) VALUES
(2, 'احمر', 'Red', '#f72222', '2019-12-26 10:04:10', '2019-12-26 10:04:10'),
(3, 'ابيض', 'white', '#ffffff', '2020-04-21 19:31:48', '2020-04-21 19:31:48'),
(4, 'اسود', 'black', '#000000', '2020-04-21 19:33:59', '2020-04-21 19:33:59');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_name_ar`, `country_name_en`, `mob`, `code`, `logo`, `currency`, `created_at`, `updated_at`) VALUES
(3, 'مصر', 'Egypt', '002', 'EG', 'countries/hNCaIm1WTf6nZxynZl2hWawlBd5reodZQ47el1JX.jpeg', 'EGP', '2019-12-30 12:45:06', '2020-04-07 20:16:00'),
(4, 'السعوديه', 'SaudArabi', '966', 'KSA', NULL, 'SAR', '2020-01-08 14:32:59', '2020-01-08 14:33:22');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `dep_name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dep_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dep_name_ar`, `dep_name_en`, `icon`, `description`, `keyword`, `parent`, `created_at`, `updated_at`) VALUES
(1, 'الاجهزة الذكية', 'Smart Device', NULL, NULL, NULL, NULL, '2019-12-24 08:49:08', '2020-01-06 09:32:22'),
(2, 'موبيلات واجهزة لوحية', 'Mobiles And Tablets', NULL, 'موبيلات واجهزة لوحية', 'Mobiles And Tablets', 1, '2019-12-24 08:49:27', '2020-01-06 09:34:52'),
(5, 'كومبيوتر وﻻبتوب', 'Computer', NULL, NULL, NULL, 1, '2020-01-06 09:36:35', '2020-01-06 09:36:35'),
(6, 'ﻻبتوب', 'Laptop', NULL, 'ﻻبتوب', 'Laptop', 5, '2020-01-06 13:25:17', '2020-01-06 13:25:17'),
(7, 'ماك برو', 'mac pro', NULL, NULL, NULL, 6, '2020-01-06 14:12:56', '2020-01-06 14:24:03'),
(8, 'اتش بي', 'hp', NULL, NULL, NULL, 6, '2020-01-06 14:23:40', '2020-01-06 14:23:40'),
(9, 'جوارب واحذية', 'socks and shose', NULL, NULL, NULL, NULL, '2020-04-21 19:17:04', '2020-04-21 19:17:04');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `name`, `size`, `file`, `path`, `full_file`, `mime_type`, `file_type`, `relation_id`, `created_at`, `updated_at`) VALUES
(15, 'iphone.jpg', '13278', 'dJYxRFXdYIqYxlLJfcGh3RADFRVm64OY0z579fsF.jpeg', 'products/4', 'products/4/dJYxRFXdYIqYxlLJfcGh3RADFRVm64OY0z579fsF.jpeg', 'image/jpeg', 'product', 4, '2020-01-05 17:49:24', '2020-01-05 17:49:24'),
(17, 'color-wing-transparent-set_1284-8933.jpg', '34196', 'vysEqPqTniXR5dicpEiwPEbC0LqZUWba1xWVOoOt.jpeg', 'products/6', 'products/6/vysEqPqTniXR5dicpEiwPEbC0LqZUWba1xWVOoOt.jpeg', 'image/jpeg', 'product', 6, '2020-04-20 07:28:52', '2020-04-20 07:28:52'),
(18, '3lm-masr.jpg', '4791', 'ZSfwsrqYwcTVPoDpNYvhk9tBsXYRwJBiht7IsntW.jpeg', 'products/6', 'products/6/ZSfwsrqYwcTVPoDpNYvhk9tBsXYRwJBiht7IsntW.jpeg', 'image/jpeg', 'product', 6, '2020-04-20 07:28:52', '2020-04-20 07:28:52'),
(41, '5696-12.jpg', '36813', 'eNwCu3ciLfgqVOykklNSUvq8BoONHxpuWMMes2c0.jpeg', 'products/26', 'products/26/eNwCu3ciLfgqVOykklNSUvq8BoONHxpuWMMes2c0.jpeg', 'image/jpeg', 'product', 26, '2020-04-21 19:39:48', '2020-04-21 19:39:48'),
(42, '5696-12.jpg', '36813', 'D9aGKq7tS0bC9Mb9h0dQyHDGNBEf25EYVb8PLZjy.jpeg', 'products/26', 'products/26/D9aGKq7tS0bC9Mb9h0dQyHDGNBEf25EYVb8PLZjy.jpeg', 'image/jpeg', 'product', 26, '2020-04-21 19:39:50', '2020-04-21 19:39:50'),
(43, '5.jpg', '15289', 'ndbZH2Kku4Xq3xnID85aK5jg86dVunEdgxy9yzRx.jpeg', 'products/27', 'products/27/ndbZH2Kku4Xq3xnID85aK5jg86dVunEdgxy9yzRx.jpeg', 'image/jpeg', 'product', 27, '2020-04-21 19:40:38', '2020-04-21 19:40:38'),
(44, '5.jpg', '15289', 'tcogN5tPWZOKv6hQKCmYMpdbyHzTF9heerCOLd23.jpeg', 'products/27', 'products/27/tcogN5tPWZOKv6hQKCmYMpdbyHzTF9heerCOLd23.jpeg', 'image/jpeg', 'product', 27, '2020-04-21 19:40:40', '2020-04-21 19:40:40'),
(45, '5696-12.jpg', '36813', 'NsF9XZ1AsPw9Pb1HOvzDwtx9g4pnNb', 'products/28', 'products/28/NsF9XZ1AsPw9Pb1HOvzDwtx9g4pnNb.jpeg', 'image/jpeg', 'product', 28, '2020-04-22 06:53:57', '2020-04-22 06:53:57'),
(46, '5696-12.jpg', '36813', 'nDC4mC6JWiUJ8rcEue4vI7gOQKJrIE', 'products/28', 'products/28/nDC4mC6JWiUJ8rcEue4vI7gOQKJrIE.jpeg', 'image/jpeg', 'product', 28, '2020-04-22 06:53:58', '2020-04-22 06:53:58');

-- --------------------------------------------------------

--
-- Table structure for table `malls`
--

CREATE TABLE `malls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `malls`
--

INSERT INTO `malls` (`id`, `name_ar`, `name_en`, `address`, `facebook`, `twitter`, `website`, `contact_name`, `mobile`, `email`, `lat`, `lng`, `icon`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'مول مصر', 'Egypt Mall', 'giza', NULL, NULL, NULL, 'Admin', '10231231233', 'mall@mall.com', '26.847981862816894', '30.60466480255127', NULL, 3, '2020-01-08 14:31:23', '2020-01-08 14:31:23'),
(2, 'الراشد مول', 'Alrashid Mall', 'sau arabi,9992', NULL, NULL, NULL, 'test', '0127652367234', 'admin@test.com', '26.847981862816894', '30.60466480255127', NULL, 4, '2020-01-08 14:35:40', '2020-01-08 14:35:40');

-- --------------------------------------------------------

--
-- Table structure for table `mall_products`
--

CREATE TABLE `mall_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `mall_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mall_products`
--

INSERT INTO `mall_products` (`id`, `product_id`, `mall_id`, `created_at`, `updated_at`) VALUES
(25, 6, 1, '2020-04-20 08:02:12', '2020-04-20 08:02:12'),
(26, 6, 2, '2020-04-20 08:02:12', '2020-04-20 08:02:12'),
(42, 27, 1, '2020-04-21 19:42:11', '2020-04-21 19:42:11'),
(43, 26, 1, '2020-04-21 19:44:38', '2020-04-21 19:44:38'),
(51, 28, 1, '2020-04-22 10:49:57', '2020-04-22 10:49:57'),
(52, 4, 1, '2020-06-28 12:27:24', '2020-06-28 12:27:24');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name_ar`, `name_en`, `address`, `facebook`, `twitter`, `website`, `contact_name`, `mobile`, `email`, `lat`, `lng`, `icon`, `created_at`, `updated_at`) VALUES
(2, 'زارا', 'Zara', NULL, NULL, NULL, NULL, 'john doe', '1231231233', 'root@gmail.com', '26.847981862816894', '30.60466480255127', NULL, '2019-12-25 12:15:12', '2019-12-25 12:15:12'),
(3, 'انتل المساهمة المتحدة', 'Intel Corporate', 'Thailiand', NULL, NULL, NULL, 'Admin', '213123123123', 'intel@intel.com', '26.847981862816894', '30.60466480255127', NULL, '2020-01-08 12:49:20', '2020-01-08 12:49:20'),
(4, 'الامبراطور', 'lemprator', NULL, NULL, NULL, NULL, 'test', '213213123123123', 'test@test.com', '26.847981862816894', '30.60466480255127', NULL, '2020-04-21 19:26:26', '2020-04-21 19:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2014_10_12_000000_create_users_table', 1),
(13, '2014_10_12_100000_create_password_resets_table', 1),
(14, '2019_10_15_181902_create_admins_table', 1),
(15, '2019_11_12_160616_create_settings_table', 1),
(16, '2019_11_18_121919_create_files_table', 1),
(17, '2019_11_20_135329_create_countries_table', 1),
(18, '2019_11_24_224250_create_cities_table', 1),
(19, '2019_11_25_122101_state', 1),
(20, '2019_11_25_213838_create_departments_table', 1),
(23, '2019_12_24_142558_create_trade_marks_table', 2),
(26, '2019_12_24_145851_create_manufacturers_table', 3),
(27, '2019_12_25_161514_create_shippings_table', 4),
(29, '2019_12_25_183302_create_malls_table', 5),
(30, '2019_12_26_111911_create_colors_table', 6),
(31, '2019_12_26_132043_create_sizes_table', 7),
(40, '2019_12_30_104711_create_weights_table', 8),
(41, '2019_12_30_111251_create_products_table', 8),
(43, '2020_01_09_155220_create_other_datas_table', 9),
(44, '2020_01_12_163128_create_mall_products_table', 10),
(45, '2020_04_22_090313_create_related_products_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `other_datas`
--

CREATE TABLE `other_datas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `data_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `other_datas`
--

INSERT INTO `other_datas` (`id`, `product_id`, `data_key`, `data_value`, `created_at`, `updated_at`) VALUES
(34, 6, 'test', 'test', '2020-04-20 08:02:12', '2020-04-20 08:02:12'),
(35, 4, 'test', 'testval', '2020-06-28 12:27:25', '2020-06-28 12:27:25'),
(36, 4, 'test1', 'eeeeeeee', '2020-06-28 12:27:25', '2020-06-28 12:27:25');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `trade_id` int(10) UNSIGNED DEFAULT NULL,
  `manu_id` int(10) UNSIGNED DEFAULT NULL,
  `color_id` int(10) UNSIGNED DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_id` int(10) UNSIGNED DEFAULT NULL,
  `currency_id` int(10) UNSIGNED DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL DEFAULT '0',
  `start_at` date DEFAULT NULL,
  `end_at` date DEFAULT NULL,
  `start_offer_at` date DEFAULT NULL,
  `end_offer_at` date DEFAULT NULL,
  `price_offer` int(11) DEFAULT '0',
  `other_data` longtext COLLATE utf8mb4_unicode_ci,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight_id` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('pending','refused','active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `reason` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `photo`, `content`, `department_id`, `trade_id`, `manu_id`, `color_id`, `size`, `size_id`, `currency_id`, `price`, `stock`, `start_at`, `end_at`, `start_offer_at`, `end_offer_at`, `price_offer`, `other_data`, `weight`, `weight_id`, `status`, `reason`, `created_at`, `updated_at`) VALUES
(4, 'test1l', NULL, 'test', 1, 1, 3, 2, '20*20', NULL, NULL, 2300, 50, '2020-01-03', '2020-01-05', '2020-04-19', '2020-01-26', 2000, '', '5', NULL, 'pending', NULL, '2019-12-31 10:10:07', '2020-06-28 12:27:25'),
(6, 'كمبيوتر ﻻبتوب', 'products/6/5nTP4scR8UROZlBarTPsfioyjQzb0yPVhmbrsK9A.png', 'تجربه', 5, 1, 3, 2, '0', NULL, NULL, 100, 60, '2020-05-11', '2020-06-02', '2020-05-15', '2020-05-28', 80, '', '0', NULL, 'pending', NULL, '2020-04-20 07:25:47', '2020-04-21 01:28:05'),
(26, 'جوارب رجالي شتوية 1x ابيض', 'products/26/RTU0EOWhADQc9TmTUK8rdl15Yev7JE086pyjPCmy.jpeg', 'جوارب رجالي شتوية', 9, 4, 4, 3, '1x', 6, NULL, 30, 400, '2020-05-05', '2020-06-05', '2020-05-07', '2020-05-31', 25, NULL, '0', 1, 'pending', NULL, '2020-04-21 19:15:12', '2020-04-21 19:44:38'),
(27, 'جوارب رجالي شتوية 1x اسود', 'products/27/YaiY0h4F52QmBO2SJvvpmibl99mB4nYNWV1pU7b6.jpeg', 'جوارب رجالي شتوية اسود', 9, 4, 4, 4, NULL, 6, NULL, 30, 400, '2020-05-05', '2020-06-05', '2020-05-07', '2020-05-31', 25, NULL, '0', 1, 'pending', NULL, '2020-04-21 19:32:30', '2020-04-21 19:42:11'),
(28, 'جوارب رجالي شتوية 1x احمر', 'products/28/DSQF8b1qQNK05kpKFYZeFBtehwgNne.jpeg', 'جوارب رجالي شتوية', 9, 4, 4, 2, NULL, 6, NULL, 30, 400, '2020-05-05', '2020-06-05', '2020-05-07', '2020-05-31', 25, NULL, '0', 1, 'pending', NULL, '2020-04-22 06:53:57', '2020-04-22 10:49:57'),
(29, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 'pending', NULL, '2020-06-28 08:05:18', '2020-06-28 08:05:18');

-- --------------------------------------------------------

--
-- Table structure for table `related_products`
--

CREATE TABLE `related_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `related_product` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `related_products`
--

INSERT INTO `related_products` (`id`, `product_id`, `related_product`, `created_at`, `updated_at`) VALUES
(7, 28, 26, '2020-04-22 10:49:57', '2020-04-22 10:49:57'),
(8, 28, 27, '2020-04-22 10:49:57', '2020-04-22 10:49:57');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `sitename_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ar',
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `keywords` longtext COLLATE utf8mb4_unicode_ci,
  `status` enum('open','close') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `message_maintenance` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `sitename_ar`, `sitename_en`, `logo`, `icon`, `email`, `main_lang`, `description`, `keywords`, `status`, `message_maintenance`, `created_at`, `updated_at`) VALUES
(1, 'ar', 'en', 'settings/eqZj69vsPSdODuWf8BWKO19WxlsCaItmbg6pH6an.jpeg', NULL, 'admin@test.com', 'ar', 'test', 'test', 'open', 'test', '2019-12-24 08:41:47', '2020-04-07 17:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `name_ar`, `name_en`, `user_id`, `lat`, `lng`, `icon`, `created_at`, `updated_at`) VALUES
(2, 'على بابا', 'Ali Baba', 1, '26.847981862816894', '30.60466480255127', NULL, '2019-12-25 15:15:42', '2019-12-25 15:15:42');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_public` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name_ar`, `name_en`, `is_public`, `department_id`, `created_at`, `updated_at`) VALUES
(3, 'بوصة', 'inche', 'yes', 1, '2020-01-06 09:38:27', '2020-01-06 09:38:27'),
(4, 'ملي متر', 'mm', 'yes', 7, '2020-01-08 10:58:12', '2020-01-08 10:58:12'),
(5, 'سنتي متر', 'cm', 'no', 8, '2020-01-08 10:58:52', '2020-01-08 10:58:52'),
(6, 'رجالي 1x', 'mens 1x', 'no', 9, '2020-04-21 19:23:56', '2020-04-21 19:23:56');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `state_name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name_ar`, `state_name_en`, `country_id`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'مدينة نصر', 'Nasr City', 3, 1, '2020-01-16 11:57:31', '2020-01-16 11:57:31');

-- --------------------------------------------------------

--
-- Table structure for table `trade_marks`
--

CREATE TABLE `trade_marks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trade_marks`
--

INSERT INTO `trade_marks` (`id`, `name_ar`, `name_en`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'ابل', 'Apple', 'trademarks/zrzW2aGRUsyALiN1FYDBtxsq4ib35PDHF0kAoh5t.jpeg', '2019-12-24 12:42:11', '2019-12-24 12:42:11'),
(2, 'اسوس', 'Asus', NULL, '2020-01-08 12:38:55', '2020-01-08 12:38:55'),
(3, 'انتل', 'intel', NULL, '2020-01-08 12:39:38', '2020-01-08 12:39:38'),
(4, 'الامبراطور', 'lemprator', NULL, '2020-04-21 19:27:27', '2020-04-21 19:27:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('user','vendor','company') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'شركة شحن 1', 'ship@ship.com', '$2y$10$6QWomu9.pXClO5m0kdI3puRvP0EN.ItEvugHme035tCzGuHBSPvjG', 'company', NULL, '2019-12-25 14:04:54', '2019-12-25 14:04:54'),
(2, 'karim', 'karim@test.com', '$2y$10$Z6LQCMx79bolgoqb.lwdPeAYh3Y8infNTdbhtcvfRb49KYKSeta.y', 'user', NULL, '2020-01-15 09:18:30', '2020-01-16 14:23:42');

-- --------------------------------------------------------

--
-- Table structure for table `weights`
--

CREATE TABLE `weights` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `weights`
--

INSERT INTO `weights` (`id`, `name_ar`, `name_en`, `created_at`, `updated_at`) VALUES
(1, 'كيلو جرام', 'Kilo Gram', '2020-01-06 09:40:13', '2020-01-06 09:42:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_country_id_foreign` (`country_id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `departments_parent_foreign` (`parent`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `malls`
--
ALTER TABLE `malls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `malls_country_id_foreign` (`country_id`);

--
-- Indexes for table `mall_products`
--
ALTER TABLE `mall_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mall_products_product_id_foreign` (`product_id`),
  ADD KEY `mall_products_mall_id_foreign` (`mall_id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_datas`
--
ALTER TABLE `other_datas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `other_datas_product_id_foreign` (`product_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_department_id_foreign` (`department_id`),
  ADD KEY `products_trade_id_foreign` (`trade_id`),
  ADD KEY `products_manu_id_foreign` (`manu_id`),
  ADD KEY `products_color_id_foreign` (`color_id`),
  ADD KEY `products_size_id_foreign` (`size_id`),
  ADD KEY `products_currency_id_foreign` (`currency_id`),
  ADD KEY `products_weight_id_foreign` (`weight_id`);

--
-- Indexes for table `related_products`
--
ALTER TABLE `related_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `related_products_product_id_foreign` (`product_id`),
  ADD KEY `related_products_related_product_foreign` (`related_product`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shippings_user_id_foreign` (`user_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sizes_department_id_foreign` (`department_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `states_country_id_foreign` (`country_id`),
  ADD KEY `states_city_id_foreign` (`city_id`);

--
-- Indexes for table `trade_marks`
--
ALTER TABLE `trade_marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `weights`
--
ALTER TABLE `weights`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `malls`
--
ALTER TABLE `malls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mall_products`
--
ALTER TABLE `mall_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `other_datas`
--
ALTER TABLE `other_datas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `related_products`
--
ALTER TABLE `related_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trade_marks`
--
ALTER TABLE `trade_marks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `weights`
--
ALTER TABLE `weights`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `malls`
--
ALTER TABLE `malls`
  ADD CONSTRAINT `malls_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mall_products`
--
ALTER TABLE `mall_products`
  ADD CONSTRAINT `mall_products_mall_id_foreign` FOREIGN KEY (`mall_id`) REFERENCES `malls` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `mall_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `other_datas`
--
ALTER TABLE `other_datas`
  ADD CONSTRAINT `other_datas_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `products_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_manu_id_foreign` FOREIGN KEY (`manu_id`) REFERENCES `manufacturers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_size_id_foreign` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_trade_id_foreign` FOREIGN KEY (`trade_id`) REFERENCES `trade_marks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_weight_id_foreign` FOREIGN KEY (`weight_id`) REFERENCES `weights` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `related_products`
--
ALTER TABLE `related_products`
  ADD CONSTRAINT `related_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `related_products_related_product_foreign` FOREIGN KEY (`related_product`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shippings`
--
ALTER TABLE `shippings`
  ADD CONSTRAINT `shippings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sizes`
--
ALTER TABLE `sizes`
  ADD CONSTRAINT `sizes_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
