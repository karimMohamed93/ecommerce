<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'Maintenance'],function(){

	Route::get('/', function () {
	    return view('style.home');
	});
});

Route::get('Maintenance',function(){

	if(setting()->status == 'open'){
            return redirect('/');
        }
	return view('style.maintenance');
});

/*Route::get('delete_file',function(){
Storage::delete('image/6CjjnXcEY3o2SLtKRF68uou0RHiXDjL0u9NgQ4CJ.jpeg');
});*/