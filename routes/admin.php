<?php

Route::group(['prefix'=>'admin','namespace' => 'Admin'],function(){

	Config::set('auth.defaults','admin');
	//Config::set("auth. defaults.passwords","admins");

	Route::get('login','AdminAuth@login');
	Route::post('login','AdminAuth@dologin');
	Route::get('forgot/password','AdminAuth@forgot_password');
	Route::post('forgot/password','AdminAuth@forgot_password_post');
	Route::get('reset/password/{token}','AdminAuth@reset_password');
	Route::post('reset/password/{token}','AdminAuth@reset_password_final');

	  Route::group(['middleware' => 'admin:admin'],function(){

	  	Route::resource('admin', 'AdminController');
	  	Route::delete('admin/destroy/all', 'AdminController@multidelete');

	  	Route::resource('users', 'UsersController');
	  	Route::delete('users/destroy/all', 'UsersController@multidelete');

	  	Route::resource('countries', 'CountriesController');
	  	Route::delete('countries/destroy/all', 'CountriesController@multidelete');

	  	Route::resource('cities', 'CitiesController');
	  	Route::delete('cities/destroy/all', 'CitiesController@multidelete');

	  	Route::resource('states', 'StatesController');
	  	Route::delete('states/destroy/all', 'StatesController@multidelete');

	  	Route::resource('trademarks', 'TradeMarksController');
	  	Route::delete('trademarks/destroy/all', 'TradeMarksController@multidelete');

	  	Route::resource('manufacturers', 'ManufacturersController');
	  	Route::delete('manufacturers/destroy/all', 'ManufacturersController@multidelete');

	  	Route::resource('shipping', 'ShippingController');
	  	Route::delete('shipping/destroy/all', 'ShippingController@multidelete');

	  	Route::resource('malls', 'MallsController');
	  	Route::delete('malls/destroy/all', 'MallsController@multidelete');

	  	Route::resource('colors', 'ColorsController');
	  	Route::delete('colors/destroy/all', 'ColorsController@multidelete');

	  	Route::resource('weights', 'WeightsController');
	  	Route::delete('weights/destroy/all', 'WeightsController@multidelete');

	  	Route::resource('sizes', 'SizesController');
	  	Route::delete('sizes/destroy/all', 'SizesController@multidelete');

	  	Route::resource('products', 'ProductsController');
	  	Route::delete('products/destroy/all', 'ProductsController@multidelete');

	  	Route::post('upload/image/{pid}','ProductsController@upload_file');
	  	Route::post('products/copy/{pid}','ProductsController@copy_product');
	  	Route::post('products/search','ProductsController@product_search');

	  	Route::post('delete/image', 'ProductsController@delete_file');

	  	Route::post('update/image/{pid}','ProductsController@update_product_image');

	  	Route::post('delete/product/image/{pid}','ProductsController@delete_main_image');

	  	Route::post('load/weight/size','ProductsController@prepare_weight_size');
	  	
	  	

	  	Route::resource('departments', 'DepartmentsController');



		 Route::get('/',function(){
		 	 
		 return view('admin.home');
	     });
	     
		 Route::get('settings', 'Settings@setting');
		 Route::post('settings', 'Settings@setting_save');

	     Route::any('logout','AdminAuth@logout');


	});

	 Route::get('lang/{lang}',function($lang){

	 	session()->has('lang')?session()->forget('lang'):'';
	 	
	 	$lang == 'ar'?session()->put('lang','ar'):session()->put('lang','en');

	 	return back();

	 });
});
