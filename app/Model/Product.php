<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
        

class Product extends Model
{
    //
    protected $table = 'products';
    protected $fillable = [
    	'reason',
    	'status',
    	'weight_id',
    	'weight',
    	'other_data',
    	'price_offer',
    	'end_offer_at',
    	'start_offer_at',
    	'end_at',
    	'start_at',
    	'stock',
    	'price',
    	'currency_id',
    	'size_id',
    	'color_id',
    	'manu_id',
    	'trade_id',
    	'department_id',
    	'content',
    	'photo',
    	'title',

    ];
    
    public function related(){

        return $this->hasMany(\App\Model\RelatedProduct::class,'product_id','id');
    }

    public function mall_product(){

        return $this->hasMany(\App\Model\MallProduct::class,'product_id','id');
    }

    public function other_data(){

        return $this->hasMany(\App\Model\OtherData::class,'product_id','id');
    }

    public function malls(){

        return $this->hasMany(\App\Model\MallProduct::class,'product_id','id');
    }

    public function files(){

     return $this->hasMany('App\File', 'relation_id', 'id')->where('file_type', 'product');

    }
}
