<?php

namespace App\DataTables;

//use App\AdminDatatable;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use App\Model\Manufacturers;

class ManuFactsDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        
            //->eloquent($query)
        return datatables($query)
            ->addColumn('edit', 'admin.manufacturers.btn.edit')
            ->addColumn('delete', 'admin.manufacturers.btn.delete')
            ->addColumn('checkbox', 'admin.manufacturers.btn.checkbox')
            ->rawColumns([
                'edit',
                'delete',
                'checkbox',
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\AdminDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        //return $model->newQuery();
        return Manufacturers::query();
        
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    //->setTableId('admindatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->addAction(['width' => '80px'])
                    //->dom('Bfrtip')
                    //->orderBy(1)
                    /*->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );*/
                    ->parameters([
                        'dom'           => 'Blfrtip',
                        'lengthMenu'    => [[10, 25, 50, 100], [10, 25, 50, trans('admin.all_record')]],
                        'buttons'       => [
                            [
                                'text' => '<i class="fa fa-plus"></i> '.trans('admin.add'),
                                'className' => 'btn btn-info',"action" => "function(){
                                    window.location.href= '".\URL::current()."/create';
                                }"],
                            ['extend' => 'print', 'className' => 'btn btn-primary', 'text' =>
                            '<i class="fa fa-print"></i>'],
                            ['extend' => 'csv', 'className' => 'btn btn-info', 'text' =>
                            '<i class="fa fa-file"></i>'.trans('admin.ex_csv')],
                            ['extend' => 'excel', 'className' => 'btn btn-success', 'text' =>
                            '<i class="fa fa-file"></i>'.trans('admin.ex_excel'),],
                            ['extend' => 'reload', 'className' => 'btn btn-default', 'text' =>
                            '<i class="fa fa-refresh"></i>'],
                            [
                                'text' => '<i class="fa fa-trash"></i>' ,
                                'className' => 'btn btn-danger delBtn'

                            ],
                            
                        ],
                        'initComplete'=>"function () {
                          this.api().columns([2,3]).every(function () {
                            var column = this;
                            var input = document.createElement(\"input\");
                            $(input).appendTo($(column.footer()).empty())
                            .on('keyup', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                        });
                      }",
                      'language' => datatable_lang(),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return[
            ['name' => 'checkbox',
            'data' => 'checkbox',
            'title' => '<input type="checkbox" class="check_all" onclick="check_all()" />',
            'exportable' => false,
            'printable' => false,
            'orderable' => false,
            'searchable' => false,
            ],
            ['name' => 'id',
            'data' => 'id',
            'title' => 'ID',
            ],
            ['name' => 'name_ar',
            'data' => 'name_ar',
            'title' => trans('admin.name_ar'),            
            ],
             ['name' => 'name_en',
            'data' => 'name_en',
            'title' => trans('admin.name_en'),            
            ],
            ['name' => 'mobile',
            'data' => 'mobile',
            'title' => trans('admin.mobile'),
            ],
            ['name' => 'contact_name',
            'data' => 'contact_name',
            'title' => trans('admin.contact_name'),
            ],
            ['name' => 'created_at',
            'data' => 'created_at',
            'title' => trans('admin.created_at'),
            ],
            ['name' => 'updated_at',
            'data' => 'updated_at',
            'title' => trans('admin.updated_at'),
            ],
            ['name' => 'edit',
            'data' => 'edit',
            'title' => trans('admin.edit'),
            'exportable' => false,
            'printable' => false,
            'orderable' => false,
            'searchable' => false,
            ],
            ['name' => 'delete',
            'data' => 'delete',
            'title' => trans('admin.delete'),
            'exportable' => false,
            'printable' => false,
            'orderable' => false,
            'searchable' => false,
            ],
        ];
        /*return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('id'),
            Column::make('name'),
            Column::make('email'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];*/
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'manufacturers_' . date('YmdHis');
    }
}
