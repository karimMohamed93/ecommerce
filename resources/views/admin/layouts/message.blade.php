@if(count($errors->all()) > 0)
	@foreach($errors->all() as $error)
		<div class="alert alert-danger">
			<ul>
				<li>{{ $error }}</li>
			</ul>	
		</div>
	@endforeach
@endif

@if(session()->has('success'))
	<div class="alert alert-success">
		<h2> {{ session('success') }}</h2>
	</div>
@endif

@if(session()->has('error'))
	<div class="alert alert-danger">
		<h2> {{ session('error') }}</h2>
	</div>
@endif